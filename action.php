<?php
/**
 * DokuWiki Plugin groupforip (Action Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Jörn <joern@starruss.de>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) {
    die();
}

class action_plugin_groupforip extends DokuWiki_Action_Plugin
{

    /**
     * Registers a callback function for a given event
     *
     * @param Doku_Event_Handler $controller DokuWiki's event controller object
     *
     * @return void
     */
    public function register(Doku_Event_Handler $controller)
    {
        $controller->register_hook('AUTH_ACL_CHECK', 'BEFORE', $this, 'handle_auth_acl_check');
        $ip_dn = $this->getConf('ip');
        if (preg_match("/\d+\.\d+\.\d+\.\d+/",$ip_dn))
           $this->priv_ip = $ip_dn;
        else
           $this->priv_ip = gethostbyname($ip_dn);
    }

    /**
     * [Custom event handler which performs action]
     *
     * Called for event:
     *
     * @param Doku_Event $event  event object by reference
     * @param mixed      $param  [the parameters passed as fifth argument to register_hook() when this
     *                           handler was registered]
     *
     * @return void
     */
    public function handle_auth_acl_check(Doku_Event &$event, $param)
    {
		$id     =& $event->data['id'];
		$user   =& $event->data['user'];
		$groups =& $event->data['groups'];

		// Inject a given group if request comes from a certain host
		if ($this->priv_ip == $_SERVER['REMOTE_ADDR'] or $this->priv_ip == $_SERVER['HTTP_X_FORWARDED_FOR']) {
			$groups[] = $this->getConf('group');
// 			dbg( $event->data);
		}

		return true;
    }

}


<?php
/**
 * Options for the groupforip plugin
 *
 * @author Jörn <joern@starruss.de>
 */


$lang['ip'] = 'Privileged IP or DN';
$lang['group'] = 'Group membership to be granted to visitors from IP/DN';
